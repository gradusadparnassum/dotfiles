(setq-default inhibit-startup-screen t)
(setq inhibit-splash-screen t)
(setq inhibit-startup-message t)
(setq initial-scratch-message "")
(setq-default indent-tabs-mode nil)
(global-set-key [mouse-4] 'next-line)
(global-set-key [mouse-5] 'previous-line)

(global-set-key (kbd "C-c q") 'auto-fill-mode)
; (global-set-key (kbd "C-c C-j") 'window-number-switch) 

(add-to-list 'load-path "~/bonjour/emacs")
(require 'use-package)
(require 'bonjour-mode)
(require 'bind-key)

(use-package ido
  :config (progn
            (add-to-list 'completion-ignored-extensions "go")
            (setq ido-ignore-extensions t)
            (ido-mode t)))

(use-package which-key)
(use-package mouse
  :config (xterm-mouse-mode t))

(use-package mwheel
  :functions mouse-wheel-mode
  :config (mouse-wheel-mode t))

;;; (use-package window-number :config (window-number-mode 1))

(defmacro add-indents (&rest pairs)
  `(progn
     ,@(mapcar (lambda (p)
                 `(put ',(car p)
                       'scheme-indent-function
                       ',(cadr p)))
               pairs)))

(use-package paredit
  :hook ((emacs-lisp-mode
          lisp-mode
          eval-expression-minibuffer-setup
          scheme-mode
          geiser-repl-mode) . enable-paredit-mode)
  :hook (scheme-mode . (lambda ()
                         (add-indents
                          (test-group 1)
                          (if 1)
                          (fn 1)
                          (for 1)
                          (defn 1)
                          (test-group 1)
                          (az/fact 1)
                          (letfn 1)
                          (if-let 1)
                          (when-let 1)
                          (bind 1)
                          (bind-methods 1)))))

(add-to-list 'auto-mode-alist '("\\.bo\\'" . bonjour-mode))
(require 'cc-mode)
(use-package org
  :bind (("C-c l" . 'org-store-link)
         ("C-c a" . 'org-agenda))
  :config (progn
            (add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
	    (setq org-directory "~/notes")
	    (setq org-default-notes-file (concat org-directory "/inbox.org"))
	    (setq org-todo-keywords
                  '((sequence "TODO(t)" "STARTED(s)" "WAITING(w)" "|" "DONE(d)" "CANCELED(c)")))))

(use-package yaml-mode)

(use-package org-bullets
  :functions org-bullets-mode
  :hook ((org-mode . (lambda () (org-bullets-mode 1)))))

(use-package org-roam
  :config (progn
            (setq org-roam-directory "~/notes/roam"))
  :hook (after-init . org-roam-mode))

(use-package company
  :bind ("C-. /" . company-complete)
  :hook '((after-init-hook . 'org-roam-mode)
          (prog-mode . company-mode)) 
  :config  (progn
             (setq company-dabbrev-downcase nil)
             (setq company-dabbrev-ignore-case nil)
             (setq company-show-numbers t)))

(use-package geiser
  :defer t
  :hook (scheme-mode . geiser-mode)
  :config (setq geiser-active-implementations '(guile))
  :commands geiser-mode)

(use-package slime)

(use-package projectile
  :config
  (progn
    (setq-default projectile-completion-system 'ido)
    (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
    (projectile-mode 1)))

(use-package monky)
(use-package rg)

(use-package whitespace-cleanup
  :hook ((scheme-mode . whitespace-cleanup-mode)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files
   '("~/goals/guile/undocumented.org" "~/notes/inbox.org" "~/notes/bonjour.org" "~/notes/priv.org" "~/.todo-list.org"))
 '(org-roam-directory "~/org-roam")
 '(package-selected-packages '(org-roam)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(use-package systemd)

;;; theme
(load-theme 'spacemacs-dark t)


;;; backup

(setq version-control t ;; Use version numbers for backups.
      kept-new-versions 10 ;; Number of newest versions to keep.
      kept-old-versions 0 ;; Number of oldest versions to keep.
      delete-old-versions t ;; Don't ask to delete excess backup versions.
      backup-by-copying t) ;; Copy all files, don't rename them.
(setq backup-directory-alist '(("" . "~/.emacs.d/backup/per-save")))
